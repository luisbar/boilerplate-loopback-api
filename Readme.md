# Instalation

```
1 git clone https://gitlab.com/luisbar/boilerplate-loopback-api boilerplate-api
2 cd /path/to/boilerplate-api
3 npm install
4 export NODE_ENV="development" (MacOS and Linux)
5 create the global-config.js file into server folder, and add this

  //Data for deploying the API
  const DEVELOPMENT_PORT = 3002;
  const DEVELOPMENT_HOST = 'localhost';
  const PRODUCTION_PORT = 3003;
  const PRODUCTION_HOST = 'localhost';

  //Data for deploying the database
  const DEVELOPMENT_DATABASE_MANAGER = 'mysql';
  const DEVELOPMENT_PORT_OF_DATABASE = 3306;
  const DEVELOPMENT_HOST_OF_DATABASE = 'localhost';
  const DEVELOPMENT_DATABASE_USER = 'user';
  const DEVELOPMENT_DATABASE_PASSWORD = 'password';
  const DEVELOPMENT_DATABASE_NAME = 'boilerplate_db';

  const PRODUCTION_DATABASE_MANAGER = 'mysql';
  const PRODUCTION_PORT_OF_DATABASE = 3306;
  const PRODUCTION_HOST_OF_DATABASE = 'localhost';
  const PRODUCTION_DATABASE_USER = 'user';
  const PRODUCTION_DATABASE_PASSWORD = 'password';
  const PRODUCTION_DATABASE_NAME = 'boilerplate_db';

  //Email
  let EMAIL_OPTIONS = {
    type: 'email',//Only email by now
    from: 'example@gmail.com',//It is required, but it won't affect if you put another email address
    to: '',
    subject: '',
    text: '',
    template: '',
    redirect: '/',
  };

  const EMAIL_PROTOCOL_TYPE = 'SMTP';
  const EMAIL_HOST = 'smtp.gmail.com';
  const EMAIL_SECURE = true;
  const EMAIL_PORT = 465;
  const EMAIL_USER = 'example@gmail.com';
  const EMAIL_PASSWORD = 'amazingpassword';
  
  module.exports = {
    EMAIL_OPTIONS,
    DEVELOPMENT_PORT,
    DEVELOPMENT_HOST,
    PRODUCTION_PORT,
    PRODUCTION_HOST,
    DEVELOPMENT_DATABASE_MANAGER,
    DEVELOPMENT_PORT_OF_DATABASE,
    DEVELOPMENT_HOST_OF_DATABASE,
    DEVELOPMENT_DATABASE_USER,
    DEVELOPMENT_DATABASE_PASSWORD,
    DEVELOPMENT_DATABASE_NAME,
    PRODUCTION_DATABASE_MANAGER,
    PRODUCTION_PORT_OF_DATABASE,
    PRODUCTION_HOST_OF_DATABASE,
    PRODUCTION_DATABASE_USER,
    PRODUCTION_DATABASE_PASSWORD,
    PRODUCTION_DATABASE_NAME,
    EMAIL_PROTOCOL_TYPE,
    EMAIL_HOST,
    EMAIL_SECURE,
    EMAIL_PORT,
    EMAIL_USER,
    EMAIL_PASSWORD,
  };

6 create a database with the name in global-config.js file
7 node .
8 open in the browser http://localhost:3002/explorer/
```

:warning: It was tried with version 5 of Mysql, please use it in order to avoid problems

:warning: You have to create a password for an application in order to avoid problems using the email address that you put, more information [here](https://support.google.com/accounts/answer/185833?p=InvalidSecondFactor&visit_id=637241353860960926-4158861672&rd=1)

# Structure of server folder

```
  server/
    boot/
      authentication.js
      automigrator.js
      createAdmin.js
      root.js
    middleware/
      errorHandler.js
    models/
      person/
        email/
          validator.js
        firstName/
          validator.js
        lastName/
          validator.js
        password/
          validator.js
        username/
          validator.js
      person.js
      person.json
    view/
      emailVerification/
        verify.ejs
    component-config.json
    config.js
    config.json
    config.local.js
    datasources.json
    error.js
    middleware.development.json
    middleware.json
    model-config.json
    server.js
    string.js
```

- **Boot folder**

    Files into this folder are executed when the API is starting, these files are executed alphabetically. [more information](https://loopback.io/doc/en/lb3/Defining-boot-scripts.html)

  - authentication.js: To enable access control.

  - automigrator.js: This file converts the models to tables into database, models are linked through datasource added to   datasources.json file. [more information](https://loopback.io/doc/en/lb3/Creating-a-database-schema-from-models.html)

  - createAdmin.js: This file create the administrator role and the admin user, in addition it bind the admin user to the administrator role. [more information](https://loopback.io/doc/en/lb3/Creating-a-default-admin-user.html)

  - root.js: In this file you can define more routes, loopback is based on Express in order to define routes.

- **Middleware folder**

    Here you can add more middlewares for all HTTP requests, in addition, you have to define the middleware added into
    the middleware.json file. [more information](https://loopback.io/doc/en/lb3/Defining-middleware.html)

  - errorHandler.js: It catch all errors thrown for the API, here you can change the error message.

- **Models folder**

    This folder contains all model defined, you can add more models using the loopback-cli. [more information](https://loopback.io/doc/en/lb3/Create-new-models.html)

    - **Person folder**
    
        This folder contains custom validations for certain properties of person model, each property has a validator.js file. this modification was done in order to have a better code.
    
      - person.js: This file contains some remote hooks [more information](https://loopback.io/doc/en/lb3/Define-a-remote-hook.html) and initialize the validators.
    
      - person.json: This file contains the configuration about person model. [more information](https://loopback.io/doc/en/lb3/Model-definition-JSON-file.html)

- **View folder**

    This folder contain the views or templates for using when an email is sent.

- component-config.json: Configuration file for LoopBack components.

- config.json: Global application settings, such as the REST API root, host name and port to use, and so on. [more information](https://loopback.io/doc/en/lb3/config.json.html) It is empty because instead of it the API is using config.development.js or config.production.js.

- config.development.js: Global application settings, such as the REST API root, host name and port to use, and so on, but if the environment var called NODE_ENV is equal to 'development'.

- config.production.js: Global application settings, such as the REST API root, host name and port to use, and so on, but if the environment var called NODE_ENV is equal to 'production'.

- datasources.json: Here you can add all datasources e.g. mysql, sqlserver, mongo and so on. In addition, you can add the email configuration. It is empty because instead of it the API is using datasources.development.js or datasources.production.js.

- datasources.development.js: Here you can add all datasources e.g. mysql, sqlserver, mongo and so on. In addition, you can add the email configuration, but if the environment var called NODE_ENV is equal to 'development'.

- datasources.production.js: Here you can add all datasources e.g. mysql, sqlserver, mongo and so on. In addition, you can add the email configuration, but if the environment var called NODE_ENV is equal to 'production'.

- error.js: This file contains default error codes thrown by loopback.

- middleware.json: Middleware configuration file.

- model-config.json: Binds models to data sources and specifies whether a model is exposed over REST, among other things. [more information](https://loopback.io/doc/en/lb3/model-config.json.html)

- server.js: Main application script.

- string.js: This file contains all error messages.

# Methods

This boilerplate has many methods for the person model, but only the checked items has been tested.

- [X] create or register an user (POST)
- [X] login
- [X] logout
- [X] verify (for sending email verification)
- [X] confirm (for confirming email verification)
- [X] update data by user id (PATCH not PUT)
- [X] change password
- [ ] delete by id

# Features

- [X] Token-based authentication.
- [X] Vanity user URLs. [more information](https://loopback.io/doc/en/lb3/Making-authenticated-requests.html)
- [ ] Enable OAuth component.
- [ ] Enable Storage component. [more information](https://loopback.io/doc/en/lb3/Storage-component.html)
- [ ] Access control with multiple user models. [more information](https://loopback.io/doc/en/lb3/Authentication-authorization-and-permissions.html#access-control-with-multiple-user-models)