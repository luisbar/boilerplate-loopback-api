//Person
const LOGIN_FAILED = 'LOGIN_FAILED';
const LOGIN_FAILED_EMAIL_NOT_VERIFIED = 'LOGIN_FAILED_EMAIL_NOT_VERIFIED';
const INVALID_PASSWORD = 'INVALID_PASSWORD';
const PASSWORD_TOO_LONG = 'PASSWORD_TOO_LONG';
const PASSWORD_TOO_SHORT = 'PASSWORD_TOO_SHORT';
const INVALID_TOKEN = 'INVALID_TOKEN';
const AUTHORIZATION_REQUIRED = 'AUTHORIZATION_REQUIRED';
const USERNAME_EMAIL_REQUIRED = 'USERNAME_EMAIL_REQUIRED';

//Bucket
const VALIDATION_ERROR = 'ValidationError';

//Generic
let ERROR = {
  stack: undefined,
  statusCode: undefined,
  name: undefined,
  message: undefined,
  code: undefined,
  details: {
    context: undefined,
    codes: {},
    messages: {},
  }
};

//Error codes
const PRESENCE = 'presence';
const LENGTH = 'length';
const EXISTENCE = 'existence';

module.exports = {
  LOGIN_FAILED,
  LOGIN_FAILED_EMAIL_NOT_VERIFIED,
  INVALID_PASSWORD,
  PASSWORD_TOO_LONG,
  PASSWORD_TOO_SHORT,
  INVALID_TOKEN,
  AUTHORIZATION_REQUIRED,
  USERNAME_EMAIL_REQUIRED,
  ERROR,
  VALIDATION_ERROR,
  PRESENCE,
  LENGTH,
  EXISTENCE,
};
