/******************
 * Project import *
 ******************/
const LOGIN_FAILED = require('../error.js').LOGIN_FAILED;
const LOGIN_FAILED_EMAIL_NOT_VERIFIED = require('../error.js').LOGIN_FAILED_EMAIL_NOT_VERIFIED;
const INVALID_PASSWORD = require('../error.js').INVALID_PASSWORD;
const PASSWORD_TOO_LONG = require('../error.js').PASSWORD_TOO_LONG;
const PASSWORD_TOO_SHORT = require('../error.js').PASSWORD_TOO_SHORT;
const INVALID_TOKEN = require('../error.js').INVALID_TOKEN;
const AUTHORIZATION_REQUIRED = require('../error.js').AUTHORIZATION_REQUIRED;
const USERNAME_EMAIL_REQUIRED = require('../error.js').USERNAME_EMAIL_REQUIRED;

const TXT_1 = require('../string.js').TXT_1;
const TXT_2 = require('../string.js').TXT_2;
const TXT_3 = require('../string.js').TXT_3;
const TXT_12 = require('../string.js').TXT_12;
const TXT_13 = require('../string.js').TXT_13;
const TXT_14 = require('../string.js').TXT_14;
const TXT_15 = require('../string.js').TXT_15;
const TXT_16 = require('../string.js').TXT_16;
const TXT_17 = require('../string.js').TXT_17;
const TXT_18 = require('../string.js').TXT_18;
const TXT_21 = require('../string.js').TXT_21;

module.exports = function() {
  return function catchError(error, req, res, next) {
    if (error)
      next({
        stack: error.stack ? String(error.stack) : String(error),
        statusCode: error.statusCode,
        name: error.name,
        message: getMessageByCode(error.code) || getMessageByStatusCode(error.statusCode),
        code: error.code,
        details: error.details
      });
    else
      next();
  };
  /**
   * It returns a message by error code
   * @param  {string} code error code
   * @return {string} error message
   */
  function getMessageByCode(code) {

    switch (code) {

      case LOGIN_FAILED:
        return TXT_1;

      case LOGIN_FAILED_EMAIL_NOT_VERIFIED:
        return TXT_2;

      case INVALID_PASSWORD:
        return TXT_12;

      case PASSWORD_TOO_LONG:
        return TXT_13;

      case PASSWORD_TOO_SHORT:
        return TXT_14;

      case INVALID_TOKEN:
        return TXT_17;

      case AUTHORIZATION_REQUIRED:
        return TXT_18;

      case USERNAME_EMAIL_REQUIRED:
        return TXT_1;

      default:
        return null;
    }
  }
  /**
   * It returns a message by error status code
   * @param  {string} statusCode error status code
   * @return {string} error message
   */
  function getMessageByStatusCode(statusCode) {

    switch (statusCode) {

      case 422://Validation error
        return TXT_15;

      case 401://Authorization error (the user does not have an access token or the token sent is wrong)
        return TXT_16;

      case 400://Missing data (bad request)
        return TXT_21;

      default:
        return TXT_3;
    }
  }
};
