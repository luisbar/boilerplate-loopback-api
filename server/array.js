const DEFAULT_ACCESS_CONTROL_LIST = [
  {
    model: '*',
    principalType: 'ROLE',
    principalId: '$everyone',
    permission: 'DENY',
    accessType: '*',
  },
  {
    model: '*',
    principalType: 'ROLE',
    principalId: 'administrator',
    permission: 'ALLOW',
    accessType: '*'
  },
  {
    model: 'person',
    principalType: 'ROLE',
    principalId: '$owner',
    permission: 'ALLOW',
    property: '__get__buckets'
  },
  {
    model: 'person',
    principalType: 'ROLE',
    principalId: '$owner',
    permission: 'ALLOW',
    property: 'getBucketsAsATree'
  },
  {
    model: 'document',
    principalType: 'ROLE',
    principalId: '$owner',
    permission: 'ALLOW',
    property: 'removeDocument'
  },
  {
    model: 'document',
    principalType: 'ROLE',
    principalId: '$owner',
    permission: 'ALLOW',
    property: 'downloadDocument'
  },
  {
    model: 'bucket',
    principalType: 'ROLE',
    principalId: '$authenticated',
    permission: 'ALLOW',
    property: 'createBucket'
  },
  {
    model: 'bucket',
    principalType: 'ROLE',
    principalId: '$owner',
    permission: 'ALLOW',
    property: 'removeBucket'
  },
  {
    model: 'bucket',
    principalType: 'ROLE',
    principalId: '$owner',
    permission: 'ALLOW',
    property: 'createDocument'
  },
  {
    model: 'accessControlList',
    principalType: 'ROLE',
    principalId: 'administrator',
    permission: 'ALLOW',
    property: 'create'
  },
  {
    model: 'accessControlList',
    principalType: 'ROLE',
    principalId: 'administrator',
    permission: 'ALLOW',
    property: 'bindAccessControlWithAModel'
  },
];

module.exports = {
  DEFAULT_ACCESS_CONTROL_LIST
};
