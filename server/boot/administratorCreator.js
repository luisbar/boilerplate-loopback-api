/***********************
 * Node modules import *
 ***********************/
let path = require('path');
/******************
 * Project import *
 ******************/
let app = require(path.resolve(__dirname, '../server'));
/**
 * It creates the admin person and the administrator role, and binds
 * the admin person to the administrator role
 */
class AdministratorCreator {

  constructor() {
    this.person = app.models.person;
    this.role = app.models.Role;
    this.roleMapping = app.models.RoleMapping;
    //Listeners
    this.person.count(this.initialQueryListener.bind(this));
    this._createAdminPersonListener = this.createAdminPersonListener.bind(this);
    this._createAdministratorRoleListener = (person) => this.createAdministratorRoleListener.bind(this, person);
    this._bindAdminPersonWithAdministratorRoleListener = this.bindAdminPersonWithAdministratorRoleListener.bind(this);
  }
  /**
   * It is triggered when the query for counting persons has finished
   * @param  {object} error    error object
   * @param  {number} quantity persons quantity
   */
  initialQueryListener(error, quantity) {
    if (!quantity) this.createAdminPerson();
  }
  /**
   * It creates the admin person
   */
  createAdminPerson() {
    this.person.create([
      {
        firstName: 'admin',
        lastName: '100',
        username: 'admin',
        email: 'luisbar180492@gmail.com',
        password: 'admin123',
        emailVerified: true,
      },
    ], this._createAdminPersonListener);
  }
  /**
   * It is triggered when the admin person has been created
   * @param  {object} error error object
   * @param  {array} persons persons created
   */
  createAdminPersonListener(error, persons) {
    if (error) return console.log('Error al crear el usuario admin: ' + error);

    this.createAdministratorRole(persons[0]);
  }
  /**
   * It creates the administrator role
   * @param  {object} person admin person object
   */
  createAdministratorRole(person) {
    this.role.create({
      name: 'administrator',
      description: 'admin role'
    }, this._createAdministratorRoleListener(person));
  }
  /**
   * It is triggered when the administrator role has been created
   * @param  {object} error error object
   * @param  {object} role  role object
   */
  createAdministratorRoleListener(person, error, role) {
    if (error) return console.log('Error al crear el rol administrator: ' + error);
    this.bindAdminPersonWithAdministratorRole(role, person)
  }
  /**
   * It binds the admin person with the administrator role
   * @param  {object} role   role object
   * @param  {object} person person object
   */
  bindAdminPersonWithAdministratorRole(role, person) {
    this.roleMapping.create({
      principalType: this.roleMapping.USER,
      principalId: person.id,
      roleId: role.id,
    }, this._bindAdminPersonWithAdministratorRoleListener);
  }
  /**
   * It is triggered when the admin person has been bonded with the administrator role
   * @param  {object} error error object
   */
  bindAdminPersonWithAdministratorRoleListener(error) {
    if (error) return console.log('Error al asignar el rol administrator al usuario admin: ' + error);;
  }
}

new AdministratorCreator();
