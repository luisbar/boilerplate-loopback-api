/***********************
 * Node modules import *
 ***********************/
let path = require('path');
/******************
 * Project import *
 ******************/
let app = require(path.resolve(__dirname, '../server'));
const DEFAULT_ACCESS_CONTROL_LIST = require('../array').DEFAULT_ACCESS_CONTROL_LIST;
/**
 * It creates default acls and binds them with models
 */
class AccessControlListCreator {

  constructor() {
    this.accessControlList = app.models.accessControlList;
    //To get model names
    this.models = Object.keys(app.models).filter((model) => model.substring(0, 1) === model.substring(0, 1).toLowerCase());
  }
  /**
   * It loads the default access control list
   */
  loadDefaultAccessControlList() {
    this.isAccessControlListTableEmpty()
    .then((flag) => !flag ? this.createDefaultAccessControlList() : this.bindAccessControlWithModels())
    .then(() => this.bindAccessControlWithModels())
    .catch((error) => console.log(error))
  }
  /**
  * It checks if the accessControlList table is empty,
  * in order to populate it with the default access control list
  * @return {promise}
  */
  isAccessControlListTableEmpty() {

    return new Promise((resolve, reject) => {

      this.accessControlList.count((error, quantity) => {
        if (error) return reject(`Hubo un problema verificando si la tabla de accessControlList esta vacía: ${error}`);

        resolve(quantity);
      });
    });
  }
  /**
   * It creates the default access control list
   * @return {promise}
   */
  createDefaultAccessControlList() {

    return new Promise((resolve, reject) => {

      this.accessControlList.create(DEFAULT_ACCESS_CONTROL_LIST, (error, aclInstances) => {
        if (error) return reject(`Hubo un problema creando la lista de accesos por defecto: ${error}`);

        resolve();
      })
    });
  }
  /**
   * It binds the access control list with models
   */
  bindAccessControlWithModels() {

    this.accessControlList.find({}, (error, aclInstances) => {
      if (error) return console.log(`Hubo un problema obteniendo la lista de accesos por defecto: ${error}`);

      const genericAcls = aclInstances.filter((aclInstance, index) => aclInstance.model === '*');
      const specificAcls = aclInstances.filter((aclInstance, index) => aclInstance.model !== '*');

      specificAcls.map((aclInstance) => app.models[aclInstance.model].settings.acls.push(aclInstance));
      this.models.map((model) => genericAcls.map((genericAcl) => app.models[model].settings.acls.push(genericAcl)));
    });
  }
}

new AccessControlListCreator().loadDefaultAccessControlList();
