/***********************
 * Node modules import *
 ***********************/
let path = require('path');
/******************
 * Project import *
 ******************/
let app = require(path.resolve(__dirname, '../server'));
/**
 * It enables the access control, without it the logout does not work
 */
class AuthenticationEnabler {

  constructor() {
    app.enableAuth();
  }
};

new AuthenticationEnabler();
