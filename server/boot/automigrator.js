/***********************
 * Node modules import *
 ***********************/
let path = require('path');
/******************
 * Project import *
 ******************/
let app = require(path.resolve(__dirname, '../server'));
/**
 * It creates the tables on database from json models
 */
class Automigrator {

  constructor() {
    this.models = ['person', 'AccessToken', 'accessControlList', 'role', 'RoleMapping', 'bucket', 'document'];
    this.dataSource = app.dataSources.mysql;
  }

  databaseFromModels() {
    this.isActual()
    .then((isActual) => !isActual && this.autoupdate())
    .then((result) => console.log('Database updated: ' + result))
    .catch((error) => console.log(error));
  }

  isActual() {

    return new Promise((resolve, reject) => {
      this.dataSource.isActual(this.models, (error, isActual) => {
        if (error)
          reject(error);
        else
          resolve(isActual);
      });
    });
  }

  autoupdate() {

    return new Promise((resolve, reject) => {
      this.dataSource.autoupdate(this.models, resolve(true));
    });
  }

  automigrate() {

    return new Promise((resolve, reject) => {
      this.dataSource.automigrate(this.models, resolve);
    });
  }
}

new Automigrator().databaseFromModels();
