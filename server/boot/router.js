/***********************
 * Node modules import *
 ***********************/
let path = require('path');
/******************
 * Project import *
 ******************/
let app = require(path.resolve(__dirname, '../server'));
/**
 * It establishes routes or endpoints
 */
class Router {

  constructor() {
    this.router = app.loopback.Router();
    //Routes
    this.router.get('/verified', this.verified.bind(this));

    app.use(this.router);
  }

  verified(req, res, next) {
    res.send('Su dirección de correo fue verificada exitosamente');
  }
}

new Router();
