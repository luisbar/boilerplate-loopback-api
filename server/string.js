//Person
const TXT_1 = 'El usuario o contraseña es incorrecto' ;
const TXT_2 = 'Debe verificar su correo para poder iniciar sesión';
const TXT_4 = 'Verificación de la dirección de correo';
const TXT_5 = 'Por favor verifica tu correo presionando el siguiente enlace {href}';
const TXT_6 = 'No se pudo cerrar la sesión';
const TXT_10 = 'La dirección de correo ya esta en uso';
const TXT_11 = 'Formato incorrecto';
const TXT_12 = 'Contraseña invalida';
const TXT_13 = 'La contraseña es demasiado larga';
const TXT_14 = 'La contraseña es demasiado corta';

//Generic
const TXT_3 = 'Algo salió mal lo sentimos :(';
const TXT_7 = 'Este campo no puede tener menos de NUMBER caracteres';
const TXT_8 = 'Este campo no puede tener mas de NUMBER caracteres';
const TXT_9 = 'Este campo no puede estar vacío';
const TXT_15 = 'Hay error en los datos introducidos';
const TXT_16 = 'No tiene credencial para acceder al recurso';
const TXT_17 = 'Su credencial ha caducado, por favor inicie sesión nuevamente';
const TXT_18 = 'No tiene credencial para acceder al recurso';
const TXT_21 = 'La petición no se puede resolver por que es erronea';

//Bucket
const TXT_19 = 'El id no pertenece a un contenedor registrado en la base de datos';
const TXT_20 = 'El id no pertenece a un usuario registrado en la base de datos';

//Access control list
const TXT_22 = 'El modelo no existe';
const TXT_23 = 'La propiedad o metodo no existe';
const TXT_24 = 'Debe introducir una propiedad o un tipo de accesso';
const TXT_25 = 'Solo se permiten 4 tipos de acceso, READ, WRITE, EXECUTE o * (todos los tipos de acceso)';
const TXT_26 = 'Solo se permiten 2 tipos de permiso, ALLOW o DENY';
const TXT_27 = 'Solo se permiten 3 tipos asignación de permisos, APP, ROLE o USER';

module.exports = {
  TXT_1,
  TXT_2,
  TXT_3,
  TXT_4,
  TXT_5,
  TXT_6,
  TXT_7,
  TXT_8,
  TXT_9,
  TXT_10,
  TXT_11,
  TXT_12,
  TXT_13,
  TXT_14,
  TXT_15,
  TXT_16,
  TXT_17,
  TXT_18,
  TXT_19,
  TXT_20,
  TXT_21,
  TXT_22,
  TXT_23,
  TXT_24,
  TXT_25,
  TXT_26,
  TXT_27,
};