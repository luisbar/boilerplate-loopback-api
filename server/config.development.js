/******************
 * Project import *
 ******************/
const DEVELOPMENT_PORT = require('./global-config.js').DEVELOPMENT_PORT;
const DEVELOPMENT_HOST = require('./global-config.js').DEVELOPMENT_HOST;
const package = require('../package.json');
/*************
 * Constants *
 *************/
const version = package.version;

module.exports = {
  host: DEVELOPMENT_HOST,
  restApiRoot: `/api/v${version}`,
  isDevEnv: true,
  port: DEVELOPMENT_PORT,
  remoting: {
    context: false,
    rest: {
      handleErrors: false,
      normalizeHttpPath: false,
      xml: false,
    },
    json: {
      strict: false,
      limit: '100kb',
    },
    urlencoded: {
      extended: true,
      limit: '100kb',
    },
    cors: false,
  },
};