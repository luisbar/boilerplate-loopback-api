/***********************
 * Node modules import *
 ***********************/
var path = require('path');
/******************
 * Project import *
 ******************/
const DEVELOPMENT_DATABASE_MANAGER = require('./global-config.js').DEVELOPMENT_DATABASE_MANAGER;
const DEVELOPMENT_PORT_OF_DATABASE = require('./global-config.js').DEVELOPMENT_PORT_OF_DATABASE;
const DEVELOPMENT_HOST_OF_DATABASE = require('./global-config.js').DEVELOPMENT_HOST_OF_DATABASE;
const DEVELOPMENT_DATABASE_USER = require('./global-config.js').DEVELOPMENT_DATABASE_USER;
const DEVELOPMENT_DATABASE_PASSWORD = require('./global-config.js').DEVELOPMENT_DATABASE_PASSWORD;
const DEVELOPMENT_DATABASE_NAME = require('./global-config.js').DEVELOPMENT_DATABASE_NAME;

const EMAIL_PROTOCOL_TYPE = require('./global-config.js').EMAIL_PROTOCOL_TYPE;
const EMAIL_HOST = require('./global-config.js').EMAIL_HOST;
const EMAIL_SECURE = require('./global-config.js').EMAIL_SECURE;
const EMAIL_PORT = require('./global-config.js').EMAIL_PORT;
const EMAIL_USER = require('./global-config.js').EMAIL_USER;
const EMAIL_PASSWORD = require('./global-config.js').EMAIL_PASSWORD;

module.exports = {
  mysql: {
    connector: DEVELOPMENT_DATABASE_MANAGER,
    hostname: DEVELOPMENT_HOST_OF_DATABASE,
    port: DEVELOPMENT_PORT_OF_DATABASE,
    user: DEVELOPMENT_DATABASE_USER,
    password: DEVELOPMENT_DATABASE_PASSWORD,
    database: DEVELOPMENT_DATABASE_NAME,
  },
  email: {
    connector: 'mail',
    transports: [{
      type: EMAIL_PROTOCOL_TYPE,
      host: EMAIL_HOST,
      secure: EMAIL_SECURE,
      port: EMAIL_PORT,
      auth: {
        user: EMAIL_USER,
        pass: EMAIL_PASSWORD
      }
    }],
  },
  storage: {
    connector: 'loopback-component-storage',
    provider: 'fileSystem',
    root: path.join(__dirname, 'storage')
  },
};
