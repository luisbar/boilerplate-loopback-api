/******************
 * Project import *
 ******************/
const PRODUCTION_PORT = require('./global-config.js').PRODUCTION_PORT;
const PRODUCTION_HOST = require('./global-config.js').PRODUCTION_HOST;
const package = require('../package.json');
/*************
 * Constants *
 *************/
const version = package.version;

module.exports = {
  host: PRODUCTION_HOST,
  restApiRoot: `/api/v${version}`,
  isDevEnv: false,
  port: PRODUCTION_PORT,
  remoting: {
    context: false,
    rest: {
      handleErrors: false,
      normalizeHttpPath: false,
      xml: false,
    },
    json: {
      strict: false,
      limit: '100kb',
    },
    urlencoded: {
      extended: true,
      limit: '100kb',
    },
    cors: false,
  },
};