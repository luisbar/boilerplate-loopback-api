/***********************
 * Node modules import *
 ***********************/
var path = require('path');
/******************
 * Project import *
 ******************/
const PRODUCTION_DATABASE_MANAGER = require('./global-config.js').PRODUCTION_DATABASE_MANAGER;
const PRODUCTION_PORT_OF_DATABASE = require('./global-config.js').PRODUCTION_PORT_OF_DATABASE;
const PRODUCTION_HOST_OF_DATABASE = require('./global-config.js').PRODUCTION_HOST_OF_DATABASE;
const PRODUCTION_DATABASE_USER = require('./global-config.js').PRODUCTION_DATABASE_USER;
const PRODUCTION_DATABASE_PASSWORD = require('./global-config.js').PRODUCTION_DATABASE_PASSWORD;
const PRODUCTION_DATABASE_NAME = require('./global-config.js').PRODUCTION_DATABASE_NAME;

const EMAIL_PROTOCOL_TYPE = require('./global-config.js').EMAIL_PROTOCOL_TYPE;
const EMAIL_HOST = require('./global-config.js').EMAIL_HOST;
const EMAIL_SECURE = require('./global-config.js').EMAIL_SECURE;
const EMAIL_PORT = require('./global-config.js').EMAIL_PORT;
const EMAIL_USER = require('./global-config.js').EMAIL_USER;
const EMAIL_PASSWORD = require('./global-config.js').EMAIL_PASSWORD;

module.exports = {
  mysql: {
    connector: PRODUCTION_DATABASE_MANAGER,
    hostname: PRODUCTION_HOST_OF_DATABASE,
    port: PRODUCTION_PORT_OF_DATABASE,
    user: PRODUCTION_DATABASE_USER,
    password: PRODUCTION_DATABASE_PASSWORD,
    database: PRODUCTION_DATABASE_NAME,
  },
  email: {
    connector: 'mail',
    transports: [{
      type: EMAIL_PROTOCOL_TYPE,
      host: EMAIL_HOST,
      secure: EMAIL_SECURE,
      port: EMAIL_PORT,
      auth: {
        user: EMAIL_USER,
        pass: EMAIL_PASSWORD
      }
    }],
  },
  storage: {
    connector: 'loopback-component-storage',
    provider: 'fileSystem',
    root: path.join(__dirname, 'storage')
  },
};
