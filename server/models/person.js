/***********************
 * Node modules import *
 ***********************/
const path = require('path');
/******************
 * Project import *
 ******************/
const EMAIL_OPTIONS = require('../global-config.js').EMAIL_OPTIONS;

const FirstNameValidator = require('./person/firstName/validator.js');
const LastNameValidator = require('./person/lastName/validator.js');
const EmailValidator = require('./person/email/validator.js');
const UsernameValidator = require('./person/username/validator.js');
const PasswordValidator = require('./person/password/validator.js');

const TXT_4 = require('../string.js').TXT_4;
const TXT_5 = require('../string.js').TXT_5;
/*************
 * Constants *
 *************/
const CREATE = 'create';

const DATASOURCE_ATTACHED = 'dataSourceAttached';

const TTL = 604800;

module.exports = function(person) {
  //To delete validations of the person model
  delete person.validations.email;
  delete person.validations.username;
  delete person.validations.password;
  //To initialize custom validations
  new FirstNameValidator(person).initializeValidations();
  new LastNameValidator(person).initializeValidations();
  new EmailValidator(person).initializeValidations();
  new UsernameValidator(person).initializeValidations();
  new PasswordValidator(person).initializeValidations();
  /**
   * It is executed after create method, but only if there
   * was not an error with it
   */
  person.afterRemote(CREATE, function(ctx, personInstance, next) {
    EMAIL_OPTIONS.to = personInstance.email;
    EMAIL_OPTIONS.subject = TXT_4;
    EMAIL_OPTIONS.text = TXT_5;
    EMAIL_OPTIONS.template = path.resolve(__dirname, '../view/emailVerification/verify.ejs');
    //To send an email to the provided person email address
    personInstance.verify(EMAIL_OPTIONS, function(error, response) {
      if (error)
        next(error);
      else
        next();
    });
  });
  /**
   * It is triggered when the data source is attached to
   * the model, through it the ttl is changed
   */
  person.on(DATASOURCE_ATTACHED, model => {
    const login = person.login;

    person.login = function (credentials, include, next) {
      credentials.ttl = TTL;
      return login.call(this, credentials, include, next);
    };
  });
  /**
   * It gets buckets of a person ordered as a tree, in addition,
   * it gets the documents by bucket
   * @param  {string}   id id of buckets owner
   * @param  {function} next callback function
   */
  person.getBucketsAsATree = function(id, next) {
    let bucket = person.app.models.bucket;

    bucket.find({ where: { personId: id }, include: ['documents']}, (error, bucketInstances) => {
      if (error) return next(error);

      next(null, arrayToTree(bucketInstances));
    });
  }
  /**
   * It orders buckets as a tree
   * @param  {array} buckets array of buckets
   * @param  {object} parent parent object
   */
  function arrayToTree(buckets, parent = { id: null }) {
    var tree = {};
    //To get children of a bucket
    let childrenFiltered = buckets.filter((child) => child.parentId == parent.id);

    if (childrenFiltered.length) {

      if (parent.id == null)
        tree = childrenFiltered;
      else
        parent.children = childrenFiltered;

      childrenFiltered.map((child) => arrayToTree(buckets, child));
    }

    return tree;
  };
};
