/******************
 * Project import *
 ******************/
const TXT_9 = require('../../../string.js').TXT_9;
const TXT_8 = require('../../../string.js').TXT_8;
const TXT_20 = require('../../../string.js').TXT_20;
const LENGTH = require('../../../error.js').LENGTH;
const EXISTENCE = require('../../../error.js').EXISTENCE;
const PRESENCE = require('../../../error.js').PRESENCE;
/**
 * It executes all validations about personId property of
 * the bucket model
 */
class PersonIdValidator {

  constructor(bucket, person, personId, error) {
    this.personIdProperty = bucket.definition.properties.personId;
    this.person = person;
    this.personId = personId;
    this.error = error;
  }
  
  executeValidations() {
    this.validatePresence();
    this.validateMaxLength();
    return this.validateExistence();
  }
  
  validatePresence() {
    if (!this.personId) {
      this.error.details.codes.personId.push(PRESENCE);
      this.error.details.messages.personId.push(TXT_9);
    }
  }
  
  validateMaxLength() {
    if (this.personId && this.personId.length > this.personIdProperty.max) {
      this.error.details.codes.personId.push(LENGTH);
      this.error.details.messages.personId.push(TXT_8.replace('NUMBER', this.personIdProperty.max));
    }
  }
  
  validateExistence() {
    
    return new Promise((resolve, reject) => {
      if (!this.personId) return resolve();
      //To check if the personId exists in the database
      this.person.exists(this.personId, (error, exists) => {
        if (error) reject(error);
        
        if (!exists) {
          this.error.details.codes.personId.push(EXISTENCE);
          this.error.details.messages.personId.push(TXT_20);
        }
        resolve()
      });
    })
  }
}

module.exports = PersonIdValidator;