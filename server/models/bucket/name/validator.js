/******************
 * Project import *
 ******************/
const TXT_8 = require('../../../string.js').TXT_8;
const TXT_9 = require('../../../string.js').TXT_9;
const PRESENCE = require('../../../error.js').PRESENCE;
const LENGTH = require('../../../error.js').LENGTH;
/**
 * It executes all validations about name property of
 * the bucket model
 */
class NameValidator {

  constructor(bucket, name, error) {
    this.nameProperty = bucket.definition.properties.name;
    this.name = name;
    this.error = error;
  }
  
  executeValidations() {
    this.validatePresence();
    this.validateMaxLength();
  }
  
  validatePresence() {
    if (!this.name) {
      this.error.details.codes.name.push(PRESENCE);
      this.error.details.messages.name.push(TXT_9);
    }
  }
  
  validateMaxLength() {
    if (this.name && this.name.length > this.nameProperty.max) {
      this.error.details.codes.name.push(LENGTH);
      this.error.details.messages.name.push(TXT_8.replace('NUMBER', this.nameProperty.max));
    }
  }
}

module.exports = NameValidator;