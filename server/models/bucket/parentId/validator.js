/******************
 * Project import *
 ******************/
const TXT_8 = require('../../../string.js').TXT_8;
const TXT_19 = require('../../../string.js').TXT_19;
const LENGTH = require('../../../error.js').LENGTH;
const EXISTENCE = require('../../../error.js').EXISTENCE;
/**
 * It executes all validations about parentId property of
 * the bucket model
 */
class ParentIdValidator {

  constructor(bucket, parentId, error) {
    this.parentIdProperty = bucket.definition.properties.parentId;
    this.bucket = bucket;
    this.parentId = parentId;
    this.error = error;
  }
  
  executeValidations() {
    this.validateMaxLength();
    return this.validateExistence();
  }
  
  validateMaxLength() {
    if (this.parentId && this.parentId.length > this.parentIdProperty.max) {
      this.error.details.codes.parentId.push(LENGTH);
      this.error.details.messages.parentId.push(TXT_8.replace('NUMBER', this.parentIdProperty.max));
    }
  }
  
  validateExistence() {
    
    return new Promise((resolve, reject) => {
      if (!this.parentId) return resolve();
      //To check if the parentId exists in the database
      this.bucket.exists(this.parentId, (error, exists) => {
        if (error) reject(error);
        
        if (!exists) {
          this.error.details.codes.parentId.push(EXISTENCE);
          this.error.details.messages.parentId.push(TXT_19);
        }
        resolve()
      });
    })
  }
}

module.exports = ParentIdValidator;