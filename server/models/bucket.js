/******************
 * Project import *
 ******************/
const NameValidator = require('./bucket/name/validator.js');
const PersonIdValidator = require('./bucket/personId/validator.js');
const ParentIdValidator = require('./bucket/parentId/validator.js');
const TXT_15 = require('../error.js').TXT_15;
const VALIDATION_ERROR = require('../error.js').VALIDATION_ERROR;
let ERROR = require('../error.js').ERROR;

module.exports = function(bucket) {
  /**
   * It validates the data before execute the createBucket method
   */
  bucket.beforeRemote('createBucket', (ctx, bucketInstance, next) => {
    const body = ctx.req.body;
    let person = bucket.app.models.person;
    //To change the error object in case that an error occurred
    ERROR.statusCode = 422;
    ERROR.name = VALIDATION_ERROR;
    ERROR.message = TXT_15;
    ERROR.stack = new Error(VALIDATION_ERROR);
    ERROR.details.context = 'bucket';
    ERROR.details.codes.name = [];
    ERROR.details.codes.personId = [];
    ERROR.details.codes.parentId = [];
    ERROR.details.messages.name = [];
    ERROR.details.messages.personId = [];
    ERROR.details.messages.parentId = [];
    //To execute validations
    new NameValidator(bucket, body.name, ERROR).executeValidations();
    new PersonIdValidator(bucket, person, body.personId, ERROR).executeValidations()
    .then(() => new ParentIdValidator(bucket, body.parentId, ERROR).executeValidations())
    .then(() => {
      //To delete properties of error object if them are empty
      if (!ERROR.details.messages.name.length) {
        delete ERROR.details.messages.name;
        delete ERROR.details.codes.name;
      }
      if (!ERROR.details.messages.personId.length) {
        delete ERROR.details.messages.personId;
        delete ERROR.details.codes.personId;
      }
      if (!ERROR.details.messages.parentId.length) {
        delete ERROR.details.messages.parentId;
        delete ERROR.details.codes.parentId;
      }
      //To check if there was a validation ERROR
      if (ERROR.details.messages.name && ERROR.details.messages.name.length)
        return next(ERROR);
      if (ERROR.details.messages.personId && ERROR.details.messages.personId.length)
        return next(ERROR);
      if (ERROR.details.messages.parentId && ERROR.details.messages.parentId.length)
        return next(ERROR);
        
      next();
    })
    .catch((error) => next(error));
  })
  /**
   * It creates a bucket in order to save documents into it
   * @param  {string}   personId owner id of the bucket
   * @param  {string}   bucketName bucket name
   * @param  {string}   parentId parent bucket id
   * @param  {function} next callback function
   */
  bucket.createBucket = function(personId, bucketName, parentId, next) {
    createPhysicalBucket(bucketName)
    .then((containerInstance) => createBucketIntoDatabase(personId, bucketName, parentId))
    .then((flag) => next(null, flag))
    .catch((error) => next(error));
  }
  /**
   * It creates a physical bucket
   * @param  {string} bucketName bucket name
   * @return {promise}
   */
  function createPhysicalBucket(bucketName) {
    
    return new Promise((resolve, reject) => {
      let container = bucket.app.models.container;

      container.createContainer({ name: bucketName }, (error, containerInstance) => {
        if (error) return reject(error);
        
        resolve(containerInstance);
      });   
    });
  }
  /**
   * It creates a bucket into database
   * @param  {string} personId   person id of the bucket owner
   * @param  {string} bucketName bucket name
   * @param  {string} parentId   parent bucket name
   * @return {promise}
   */
  function createBucketIntoDatabase(personId, bucketName, parentId) {
    
    return new Promise((resolve, reject) => {
      
      bucket.create({ personId: personId, name: bucketName, parentId: parentId }, (error, bucketInstance) => {
        if (error) return reject(error);
        
        resolve(true)
      });
    });
  }
  /**
   * It destroys a bucket
   * @param  {string}   id bucket id
   * @param  {function} next callback function
   */
  bucket.removeBucket = function(id, next) {
    getBucketByBucketId(id)
    .then((bucketInstance) => removePhysicalBucket(bucketInstance.name))
    .then((containerInstance) => removeBucketFromDatabase(id))
    .then((flag) => next(null, flag))
    .catch((error) => next(error));
  }
  /**
   * It returns the bucket by its id
   * @param  {string} bucketId bucket id
   * @return {promise}
   */
  function getBucketByBucketId(bucketId) {
    
    return new Promise((resolve, reject) => {
      
      bucket.findById(bucketId, (error, bucketInstance) => {
        if (error) return reject(error);
        
        resolve(bucketInstance);
      });
    });
  }
  /**
   * It removes a physical bucket by its name
   * @param  {string} bucketName bucket name
   * @return {promise}
   */
  function removePhysicalBucket(bucketName) {
    
    return new Promise((resolve, reject) => {
      let container = bucket.app.models.container;
      
      container.destroyContainer(bucketName, (error, containerInstance) => {
        if (error) return reject(error);
        
        resolve(containerInstance)
      });
    });
  }
  /**
   * It removes a bucket from the database by its id
   * @param  {string} bucketId bucket id
   * @return {promise}
   */
  function removeBucketFromDatabase(bucketId) {
    
    return new Promise((resolve, reject) => {
      
      bucket.destroyById(bucketId, (error) => {
        if (error) return reject(error);
        
        resolve(true);
      })
    });
  }
  /**
   * It creates a document in order to be saved into a bucket
   * @param  {string}   bucketId bucket id
   * @param  {object}   req      request object
   * @param  {object}   res      response object
   * @param  {function} next     callback function
   */
  bucket.createDocument = function(bucketId, req, res, next) {
    getBucketByBucketId(bucketId)
    .then((bucketInstance) => createPhysicalDocuments(bucketInstance.name, req, res, bucketId, bucketInstance.personId))
    .then((documentsSaved) => createDocumentsIntoDatabase(documentsSaved))
    .then((flag) => next(null, flag))
    .catch((error) => next(error));
  }
  /**
   * It creates documents into a bucket by the bucket name
   * @param  {string} bucketName bucket name
   * @param  {object} req        request object
   * @param  {object} res        response object
   * @param  {string} bucketId   bucket id
   * @param  {string} personId   person id
   * @return {promise}
   */
  function createPhysicalDocuments(bucketName, req, res, bucketId, personId) {
    
    return new Promise((resolve, reject) => {
      let container = bucket.app.models.container;

      container.upload(bucketName, req, res, {}, (error, data) => {
        if (error) return reject(error);

        const anotherData = { bucketId: bucketId, personId: personId };
        const documentsSaved = data.files[''].map((documentSaved) => Object.assign(documentSaved, anotherData));

        resolve(documentsSaved);
      });
    });
  }
  /**
   * It creates documents into the database
   * @param  {array} documentsSaved  array of documents which have been created
   * @return {promise}
   */
  function createDocumentsIntoDatabase(documentsSaved) {

    return new Promise((resolve, reject) => {
      let document = bucket.app.models.document;

      document.create(documentsSaved, (error, bucketInstances) => {
        if (error) return reject(error);
        
        resolve(true);
      });
    });
  }
};
