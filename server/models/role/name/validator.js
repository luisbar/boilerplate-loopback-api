/******************
 * Project import *
 ******************/
const TXT_8 = require('../../../string.js').TXT_8;
const TXT_9 = require('../../../string.js').TXT_9;
/*************
 * Variables *
 *************/
let _this;
/**
 * It executes all validations about name property of
 * the role model
 */
class NameValidator {

  constructor(role) {
    _this = this;
    this.nameProperty = role.definition.properties.name;
    this.role = role;
  }

  initializeValidations() {
    this.role.validatesPresenceOf('name', {
      message: TXT_9
    });

    this.role.validate('name', this.validateMaximumLenght, {
      message: TXT_8.replace('NUMBER', this.nameProperty.max)
    });
  }

  validateMaximumLenght(error) {
    if (this.name && this.name.length > _this.nameProperty.max)
      error();
  }
}

module.exports = NameValidator;
