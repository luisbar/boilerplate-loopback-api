/******************
 * Project import *
 ******************/
const TXT_7 = require('../../../string.js').TXT_7;
const TXT_8 = require('../../../string.js').TXT_8;
const TXT_9 = require('../../../string.js').TXT_9;
/*************
 * Variables *
 *************/
let _this;
/**
 * It executes all validations about firstName property of
 * the person model
 */
class FirstNameValidator {

  constructor(person) {
    _this = this;
    this.firstNameProperty = person.definition.properties.firstName;
    this.person = person;
  }
  
  initializeValidations() {
    this.person.validatesPresenceOf('firstName', {
      message: TXT_9
    });
    
    this.person.validate('firstName', this.validateMinimumLenght, {
      message: TXT_7.replace('NUMBER', this.firstNameProperty.min)
    });
    
    this.person.validate('firstName', this.validateMaximumLenght, {
      message: TXT_8.replace('NUMBER', this.firstNameProperty.max)
    });
  }
  
  validateMinimumLenght(error) {
    if (this.firstName && this.firstName.length < _this.firstNameProperty.min)
      error();
  }
  
  validateMaximumLenght(error) {
    if (this.firstName && this.firstName.length > _this.firstNameProperty.max)
      error();
  }
}

module.exports = FirstNameValidator;