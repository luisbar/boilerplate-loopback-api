/***********************
 * Node modules import *
 ***********************/
var isEmail = require('isemail');
/******************
 * Project import *
 ******************/
const TXT_8 = require('../../../string.js').TXT_8;
const TXT_9 = require('../../../string.js').TXT_9;
const TXT_10 = require('../../../string.js').TXT_10;
const TXT_11 = require('../../../string.js').TXT_11;
/*************
 * Variables *
 *************/
let _this;
/**
 * It executes all validations about email property of
 * the person model
 */
class EmailValidator {

  constructor(person) {
    _this = this;
    this.emailProperty = person.definition.properties.email;
    this.person = person;
  }
  
  initializeValidations() {
    this.person.validatesPresenceOf('email', {
      message: TXT_9
    });
    
    this.person.validatesUniquenessOf('email', {
      message: TXT_10 
    });

    this.person.validate('email', this.validateFormat, {
      message: TXT_11
    });
    
    this.person.validate('email', this.validateMaximumLength, {
      message: TXT_8.replace('NUMBER', this.emailProperty.max)
    });
  }
  
  validateFormat(error) {
    if (this.email && !isEmail.validate(this.email))
      error();
  }
  
  validateMaximumLength(error) {
    if (this.email && this.email.length > _this.emailProperty.max)
      error();
  }
}

module.exports = EmailValidator;