/******************
 * Project import *
 ******************/
const TXT_7 = require('../../../string.js').TXT_7;
const TXT_8 = require('../../../string.js').TXT_8;
const TXT_9 = require('../../../string.js').TXT_9;
/*************
 * Variables *
 *************/
let _this;
/**
 * It executes all validations about lastName property of
 * the person model
 */
class LastNameValidator {

  constructor(person) {
    _this = this;
    this.lastNameProperty = person.definition.properties.lastName;
    this.person = person;
  }
  
  initializeValidations() {
    this.person.validatesPresenceOf('lastName', {
      message: TXT_9
    });
    
    this.person.validate('lastName', this.validateMinimumLenght, {
      message: TXT_7.replace('NUMBER', this.lastNameProperty.min)
    });
    
    this.person.validate('lastName', this.validateMaximumLenght, {
      message: TXT_8.replace('NUMBER', this.lastNameProperty.max)
    });
  }
  
  validateMinimumLenght(error) {
    if (this.lastName && this.lastName.length < _this.lastNameProperty.min)
      error();
  }
  
  validateMaximumLenght(error) {
    if (this.lastName && this.lastName.length > _this.lastNameProperty.max)
      error();
  }
}

module.exports = LastNameValidator;