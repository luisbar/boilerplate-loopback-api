/******************
 * Project import *
 ******************/
const INVALID_PASSWORD = require('../../../error.js').INVALID_PASSWORD;
const PASSWORD_TOO_LONG = require('../../../error.js').PASSWORD_TOO_LONG;
const PASSWORD_TOO_SHORT = require('../../../error.js').PASSWORD_TOO_SHORT;
/*************
 * Variables *
 *************/
let _this;
/**
 * It executes all validations about password property of
 * the person model
 */
class PasswordValidator {

  constructor(person) {
    _this = this;
    this.passwordProperty = person.definition.properties.password;
    this.person = person;
  }

  initializeValidations() {
    this.person.validatePassword = this.validatePassword;
  }

  validatePassword(plain) {
    let error;

    if (!plain || typeof plain !== 'string') {
      error = new Error();
      error.code = INVALID_PASSWORD;
      error.statusCode = 422;
      throw error;
    }

    if (plain.length > _this.passwordProperty.max) {
      error = new Error();
      error.code = PASSWORD_TOO_LONG;
      error.statusCode = 422;
      throw error;
    }

    if (plain.length < _this.passwordProperty.min) {
      error = new Error();
      error.code = PASSWORD_TOO_SHORT;
      error.statusCode = 422;
      throw error;
    }
  };
}

module.exports = PasswordValidator;
