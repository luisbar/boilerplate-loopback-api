/******************
 * Project import *
 ******************/
const TXT_8 = require('../../../string.js').TXT_8;
const TXT_9 = require('../../../string.js').TXT_9;
/*************
 * Variables *
 *************/
let _this;
/**
 * It executes all validations about username property of
 * the person model
 */
class UsernameValidator {

  constructor(person) {
    _this = this;
    this.usernameProperty = person.definition.properties.username;
    this.person = person;
  }
  
  initializeValidations() {
    this.person.validatesPresenceOf('username', {
      message: TXT_9
    });

    this.person.validate('username', this.validateMaximumLenght, {
      message: TXT_8.replace('NUMBER', this.usernameProperty.max)
    });
  }
  
  validateMaximumLenght(error) {
    if (this.username && this.username.length > _this.usernameProperty.max)
      error();
  }
}

module.exports = UsernameValidator;