/******************
 * Project import *
 ******************/
const TXT_8 = require('../../../string.js').TXT_8;
const TXT_23 = require('../../../string.js').TXT_23;
const TXT_24 = require('../../../string.js').TXT_24;
/*************
 * Variables *
 *************/
let _this;
/**
 * It executes all validations about property property of
 * the accessControlList model
 */
class PropertyValidator {

  constructor(accessControlList) {
    _this = this;
    this.propertyProperty = accessControlList.definition.properties.property;
    this.accessControlList = accessControlList;
    //To get model names
    this.models = Object.keys(accessControlList.app.models).filter((model) => model.substring(0, 1) === model.substring(0, 1).toLowerCase());
  }
  
  initializeValidations() {
    this.accessControlList.validate('property', this.validateMaximumLenght, {
      message: TXT_8.replace('NUMBER', this.propertyProperty.max)
    });
    
    this.accessControlList.validate('property', this.validatePresenceOfPropertyOrAccessType, {
      message: TXT_24
    });

    this.accessControlList.validate('property', this.validateIfItIsAProperty, {
      message: TXT_23
    });
  
  }
  
  validateMaximumLenght(error) {
    if (this.property && this.property.length > _this.propertyProperty.max)
      error();
  }
  
  validatePresenceOfPropertyOrAccessType(error) {
    if (!this.property && !this.accessType)
      error();
  }
  
  validateIfItIsAProperty(error) {
    let properties;

    if (this.model && _this.models.concat(['*']).includes(this.model))
      properties = this.model === '*' ? _this.getPropertiesOfAllModels() : _this.getPropertiesByModelName(this.model);
      
    if (this.property && properties && !properties.includes(this.property))
      error();
  }
  
  getPropertiesByModelName(modelName) {
    let properties = [];

    _this.accessControlList.app.models[modelName].sharedClass
    .methods({ includeDisabled: true })
    .reduce((result, sharedMethod) => {
      properties.push(sharedMethod.name);
    }, {});

    return properties.concat(['*']);
  }
  
  getPropertiesOfAllModels() {
    let properties = [];
    let commonProperties;
    let arrays = [];
    let modelsWithoutContainer = this.models.filter((model) => model !== 'container');
    //To get properties of all models
    properties = modelsWithoutContainer.map((model) => _this.getPropertiesByModelName(model));
    //To make an intersection between arrays
    commonProperties = properties.reduce((a, b) => a.filter(c => b.includes(c)));
    
    return commonProperties.concat(['*']);
  }
}

module.exports = PropertyValidator;