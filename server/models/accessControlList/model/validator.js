/******************
 * Project import *
 ******************/
const TXT_8 = require('../../../string.js').TXT_8;
const TXT_9 = require('../../../string.js').TXT_9;
const TXT_22 = require('../../../string.js').TXT_22;
/*************
 * Variables *
 *************/
let _this;
/**
 * It executes all validations about model property of
 * the accessControlList model
 */
class ModelValidator {

  constructor(accessControlList) {
    _this = this;
    this.modelProperty = accessControlList.definition.properties.model;
    this.accessControlList = accessControlList;
    //To get model names
    this.models = Object.keys(accessControlList.app.models).filter((model) => model.substring(0, 1) === model.substring(0, 1).toLowerCase()).concat(['*']);
  }

  initializeValidations() {
    this.accessControlList.validatesPresenceOf('model', {
      message: TXT_9
    });

    this.accessControlList.validate('model', this.validateMaximumLenght, {
      message: TXT_8.replace('NUMBER', this.modelProperty.max)
    });

    this.accessControlList.validate('model', this.validateIfItIsAModel, {
      message: TXT_22
    });
  }

  validateMaximumLenght(error) {
    if (this.model && this.model.length > _this.modelProperty.max)
      error();
  }

  validateIfItIsAModel(error) {
    if (this.model && !_this.models.includes(this.model))
      error();
  }
}

module.exports = ModelValidator;
