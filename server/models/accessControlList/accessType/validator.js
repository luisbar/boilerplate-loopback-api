/******************
 * Project import *
 ******************/
const TXT_8 = require('../../../string.js').TXT_8;
const TXT_24 = require('../../../string.js').TXT_24;
const TXT_25 = require('../../../string.js').TXT_25;
/*************
 * Variables *
 *************/
let _this;
/**
 * It executes all validations about accessType property of
 * the accessControlList model
 */
class AccessTypeValidator {

  constructor(accessControlList) {
    _this = this;
    this.accessTypeProperty = accessControlList.definition.properties.accessType;
    this.accessControlList = accessControlList;
  }
  
  initializeValidations() {
    this.accessControlList.validate('accessType', this.validateMaximumLenght, {
      message: TXT_8.replace('NUMBER', this.accessTypeProperty.max)
    });
  
    this.accessControlList.validate('accessType', this.validatePresenceOfPropertyOrAccessType, {
      message: TXT_24
    });

    this.accessControlList.validate('accessType', this.validataInclusion, {
      message: TXT_25
    });
  }
  
  validateMaximumLenght(error) {
    if (this.accessType && this.accessType.length > _this.accessTypeProperty.max)
      error();
  }
  
  validatePresenceOfPropertyOrAccessType(error) {
    if (!this.property && !this.accessType)
      error();
  }
  
  validataInclusion(error) {
    if (this.accessType && !['READ', 'WRITE', 'EXECUTE', '*'].includes(this.accessType))
      error();
  }
}

module.exports = AccessTypeValidator;