/******************
 * Project import *
 ******************/
const TXT_8 = require('../../../string.js').TXT_8;
const TXT_9 = require('../../../string.js').TXT_9;
const TXT_27 = require('../../../string.js').TXT_27;
/*************
 * Variables *
 *************/
let _this;
/**
 * It executes all validations about principalType property of
 * the accessControlList model
 */
class PrincipalTypeValidator {

  constructor(accessControlList) {
    _this = this;
    this.principalTypeProperty = accessControlList.definition.properties.principalType;
    this.accessControlList = accessControlList;
  }
  
  initializeValidations() {
    this.accessControlList.validatesPresenceOf('principalType', {
      message: TXT_9
    });
    
    this.accessControlList.validate('principalType', this.validateMaximumLenght, {
      message: TXT_8.replace('NUMBER', this.principalTypeProperty.max)
    });
    
    this.accessControlList.validate('principalType', this.validataInclusion, {
      message: TXT_27
    });
  }
  
  validateMaximumLenght(error) {
    if (this.principalType && this.principalType.length > _this.principalTypeProperty.max)
      error();
  }
  
  validataInclusion(error) {
    if (this.principalType && !['APP', 'ROLE', 'USER'].includes(this.principalType))
      error();
  }
}

module.exports = PrincipalTypeValidator;