/******************
 * Project import *
 ******************/
const TXT_8 = require('../../../string.js').TXT_8;
const TXT_9 = require('../../../string.js').TXT_9;
/*************
 * Variables *
 *************/
let _this;
/**
 * It executes all validations about principalId property of
 * the accessControlList model
 */
class PrincipalIdValidator {

  constructor(accessControlList) {
    _this = this;
    this.principalIdProperty = accessControlList.definition.properties.principalId;
    this.accessControlList = accessControlList;
  }

  initializeValidations() {
    this.accessControlList.validatesPresenceOf('principalId', {
      message: TXT_9
    });

    this.accessControlList.validate('principalId', this.validateMaximumLenght, {
      message: TXT_8.replace('NUMBER', this.principalIdProperty.max)
    });

    this.accessControlList.validate('principalId', this.validatePrincipalIdByRole, {
      message: 'No existe el rol'
    });

    this.accessControlList.validate('principalId', this.validatePrincipalIdByApp, {
      message: 'No esta habilitada la asignación de permisos por APP'
    });

    this.accessControlList.validate('principalId', this.validatePrincipalIdByUser, {
      message: 'No existe el usuario'
    });
  }

  validateMaximumLenght(error) {
    if (this.principalId && this.principalId.length > _this.principalIdProperty.max)
      error();
  }

  validatePrincipalIdByRole(error) {
    let roles;

    if (this.principalType === 'ROLE') {
      _this.accessControlList.app.models.Role.find({}, (err, roleInstances) => {
        if (err) return error();

        roles = roleInstances.map((role) => role.name).concat(['$everyone', '$unauthenticated', '$authenticated', '$owner']);
      });

      while(!roles) require('deasync').sleep(100);

      if (this.principalId && !roles.includes(this.principalId))
        error();
    }
  }

  validatePrincipalIdByApp(error) {
    if (this.principalType === 'APP')
      error();
  }

  validatePrincipalIdByUser(error) {
    let user = undefined;

    if (this.principalType === 'USER') {
      _this.accessControlList.app.models.Person.findById(this.principalId, (err, userInstances) => {
        if (err) return error();
        user = userInstances;
      });

      while(typeof user === 'undefined') require('deasync').sleep(100);

      if (this.principalId && !user)
        error();
    }
  }
}

module.exports = PrincipalIdValidator;
