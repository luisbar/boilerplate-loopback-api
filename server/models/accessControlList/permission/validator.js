/******************
 * Project import *
 ******************/
const TXT_8 = require('../../../string.js').TXT_8;
const TXT_9 = require('../../../string.js').TXT_9;
const TXT_26 = require('../../../string.js').TXT_26;
/*************
 * Variables *
 *************/
let _this;
/**
 * It executes all validations about permission property of
 * the accessControlList model
 */
class PermissionValidator {

  constructor(accessControlList) {
    _this = this;
    this.permissionProperty = accessControlList.definition.properties.permission;
    this.accessControlList = accessControlList;
  }
  
  initializeValidations() {
    this.accessControlList.validatesPresenceOf('permission', {
      message: TXT_9
    });
    
    this.accessControlList.validate('permission', this.validateMaximumLenght, {
      message: TXT_8.replace('NUMBER', this.permissionProperty.max)
    });
    
    this.accessControlList.validate('permission', this.validataInclusion, {
      message: TXT_26
    });
  }
  
  validateMaximumLenght(error) {
    if (this.permission && this.permission.length > _this.permissionProperty.max)
      error();
  }
  
  validataInclusion(error) {
    if (this.permission && !['ALLOW', 'DENY'].includes(this.permission))
      error();
  }
}

module.exports = PermissionValidator;