/****************
 * From project *
 ****************/
const Name = require('./role/name/validator.js');

module.exports = function(role) {
  //To delete validations of the role model
  delete role.validations.name;
  //To initialize custom validations
  new Name(role).initializeValidations();
}
