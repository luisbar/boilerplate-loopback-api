module.exports = function(document) {
  /**
   * It removes a document from physical bucket and from the database
   * @param  {string}   id document id
   * @param  {function} next callback function
   */
  document.removeDocument = function(id, next) {
    getBucketIdByDocumentId(id)
    .then((bucketId) => Promise.all([getBucketNameByBucketId(bucketId), getDocumentNameByDocumentId(id)]))
    .then((names) => removeDocumentFromBucket(names[0], names[1]))
    .then((flag) => removeDocumentFromDatabase(id))
    .then((flag) => next(null, flag))
    .catch((error) => next(error));
  }
  /**
   * It downloads a document by document id
   * @param  {string}   id   document id
   * @param  {object}   req  request object
   * @param  {object}   res  response object
   * @param  {function} next callback function
   */
  document.downloadDocument = function(id, req, res, next) {
    getBucketIdByDocumentId(id)
    .then((bucketId) => Promise.all([getBucketNameByBucketId(bucketId), getDocumentNameByDocumentId(id)]))
    .then((names) => downloadDocument(names[0], names[1], req, res))
    .then((res) => res.send)
    .catch((error) => next(error));
  }
  /**
   * It returns the bucket id by document id
   * @param  {string} documentId document id
   * @return {promise}
   */
  function getBucketIdByDocumentId(documentId) {
    
    return new Promise((resolve, reject) => {
      
      document.findById(documentId, (error, documentInstance) => {
        if (error) return reject(error);
        
        resolve(documentInstance.bucketId);
      })
    });
  }
  /**
   * It returns the name of a bucket by its id
   * @param  {string} bucketId bucketId id
   * @return {promise}
   */
  function getBucketNameByBucketId(bucketId) {
    const bucket = document.app.models.bucket;
    
    return new Promise((resolve, reject) => {
      
      bucket.findById(bucketId, (error, bucketInstance) => {
        if (error) return reject(error);
        
        resolve(bucketInstance.name);
      });
    })
  }
  /**
   * It returns the name of a document by its id
   * @param  {string} documentId document id
   * @return {promise}
   */
  function getDocumentNameByDocumentId(documentId) {
    
    return new Promise((resolve, reject) => {
      
      document.findById(documentId, (error, documentInstance) => {
        if (error) return reject(error);
        
        resolve(documentInstance.name);
      });
    });
  }
  /**
   * It removes a document from the physical bucket
   * @param  {string} bucketName   bucket name
   * @param  {string} documentName document name
   * @return {promise}
   */
  function removeDocumentFromBucket(bucketName, documentName) {
    
    return new Promise((resolve, reject) => {
      const container = document.app.models.container;
      
      container.removeFile(bucketName, documentName, (error) => {
        if (error) return reject(error);
        
        resolve(true);
      }); 
    });
  }
  /**
   * It removes a document from the database
   * @param  {string} documentId document id
   * @return {promise}
   */
  function removeDocumentFromDatabase(documentId) {
    
    return new Promise((resolve, reject) => {
      
      document.destroyById(documentId, (error) => {
        if (error) return reject(error);
        
        resolve(true);
      });
    });
  }
  /**
   * It downloads a document by bucketName and documentName
   * @param  {string} bucketName   bucket name
   * @param  {string} documentName document name
   * @param  {object} req          request object
   * @param  {object} res          response object
   * @return {promise}
   */
  function downloadDocument(bucketName, documentName, req, res) {
    
    return new Promise((resolve, reject) => {
      const container = document.app.models.container;
      
      container.download(bucketName, documentName, req, res, (error) => {
        if (error) return reject(error);
        
        resolve(res);
      });
    });
  }
};
