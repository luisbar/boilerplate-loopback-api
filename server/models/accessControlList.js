/******************
 * Project import *
 ******************/
const ModelValidator = require('./accessControlList/model/validator.js');
const PropertyValidator = require('./accessControlList/property/validator.js');
const AccessTypeValidator = require('./accessControlList/accessType/validator.js');
const PermissionValidator = require('./accessControlList/permission/validator.js');
const PrincipalTypeValidator = require('./accessControlList/principalType/validator.js');
const PrincipalIdValidator = require('./accessControlList/principalId/validator.js');
/*************
 * Constants *
 *************/
const DATASOURCE_ATTACHED = 'dataSourceAttached';

module.exports = function(accessControlList) {
  //To initialize custom validations
  let modelValidator;
  let propertyValidator;
  new AccessTypeValidator(accessControlList).initializeValidations();
  new PermissionValidator(accessControlList).initializeValidations();
  new PrincipalTypeValidator(accessControlList).initializeValidations();
  new PrincipalIdValidator(accessControlList).initializeValidations();
  /**
   * It is triggered before the create method has been called, and
   * the model validator is initialized here because it need the app
   * object in order to get all model names
   */
  accessControlList.beforeRemote('create', (ctx, accessControlListInstance, next) => {
    if (!modelValidator) {
      modelValidator = new ModelValidator(accessControlList);
      modelValidator.initializeValidations();
    }
    if (!propertyValidator) {
      propertyValidator = new PropertyValidator(accessControlList);
      propertyValidator.initializeValidations();
    }

    next();
  });
  /**
   * It binds an access control with a model
   * @param  {string}   id        id of an access control
   * @param  {string}   modelName model name
   * @param  {function} next      callback function
   */
  accessControlList.bindAccessControlWithAModel = function(id, modelName, next) {

    accessControlList.findById(id, (error, accessControlInstance) => {
      if (error) return next(error);

      accessControlList.app.models[modelName].settings.acls.push(accessControlInstance);
      next(null, true);
    });
  }
};
